document.addEventListener("DOMContentLoaded", function () {
    const themeButton = document.getElementById("themeButton");
    const body = document.body;
  
    const savedTheme = localStorage.getItem("theme");
    if (savedTheme) {
      body.classList.add(savedTheme);
    }
  
    themeButton.addEventListener("click", function () {
      body.classList.toggle("dark-theme");

      const currentTheme = body.classList.contains("dark-theme") ? "dark-theme" : "";
      localStorage.setItem("theme", currentTheme);
    });
  });